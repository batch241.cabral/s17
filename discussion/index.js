function printName() {
    console.log("My name is John!");
}

const printNameTwo = () => {
    console.log("My name is John!");
};

printName();
printNameTwo();

let variableFunction = function () {
    console.log("My name is John2!");
};
variableFunction();

let funcExpression = function funcName() {
    console.log("My name is John3!");
};
funcExpression();

let func1 = function () {
    console.log("My name is John4!");
};
func1();

func1 = function () {
    console.log("My name is John5!");
};
func1();
{
    let localVar = "I am a local variable";
    console.log(localVar);
}

let globalVar = "I am global variable";

console.log(globalVar);

function showNames() {
    var functionVar = "joe";
    const functionConst = "john";
    let functionLet = "jana";

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}
showNames();

function myNewFunction() {
    let name = "maria";
    function nestedFunction() {
        let nestedName = "jose";
        console.log(name);
        console.log(nestedName);
    }
    nestedFunction();
}
myNewFunction();

let samplePrompt = prompt("Enter your name: ");
console.log("hello, \n" + typeof samplePrompt);

function try1() {}
