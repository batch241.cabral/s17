/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:
function promtUser() {
    const fullName = prompt("What is your fullname?");

    const displayInputFromPromt = () => {
        //used arrow function for the whole activity, please tell me if this is not allowed, TY
        const age = prompt("What is your age?");
        const yourLocation = prompt("What is your location?");

        console.log("Hello, " + fullName);
        console.log("You are " + age + " years old.");
        console.log("You live in " + yourLocation);
    };
    displayInputFromPromt();
    alert("Welcome " + fullName);
}
promtUser();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
function getBandList() {
    const favBandNumberThree = "3. The Eagles";
    const favBandNumberFive = "5. Eraserheads";

    const getFavoriteBand = (stringBand) => {
        console.log(stringBand);
    };
    getFavoriteBand("1. The Beatles");
    getFavoriteBand("2. Metallica");
    getFavoriteBand(favBandNumberThree);
    getFavoriteBand("4. LANY");
    getFavoriteBand(favBandNumberFive);
}
getBandList();
//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

const favMovies = [
    //created an array of objects to accomplish this one, please tell me if this is not allowed, TY
    {
        title: "1. The Godfather",
        rating: "Rotten Tomatoes Rating: 97%",
    },
    {
        title: "2. The Godfather, Part II",
        rating: "Rotten Tomatoes Rating: 96%",
    },
    {
        title: "3. Shawshank Redemption",
        rating: "Rotten Tomatoes Rating: 91%",
    },
    {
        title: "4. To Kill a Mockingbird",
        rating: "Rotten Tomatoes Rating: 93%",
    },
    {
        title: "5. Psyco",
        rating: "Rotten Tomatoes Rating: 96%",
    },
];

const displayFavoriteMovies = () => {
    favMovies.forEach((movie) => {
        console.log(movie.title);
        console.log(movie.rating);
    });
};
displayFavoriteMovies();

//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers() {
    alert("Hi! Please add the names of your friends.");
    let friend1 = prompt("Enter your first friend's name:");
    let friend2 = prompt("Enter your second friend's name:");
    let friend3 = prompt("Enter your third friend's name:");

    console.log("You are friends with:");
    console.log(friend1);
    console.log(friend2);
    console.log(friend3);
};
printFriends();

// console.log(friend1);
// console.log(friend2);
